using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    [Header("Audio Source")]
    [SerializeField] AudioSource musicSource;
    [SerializeField] AudioSource SFXSource;

    [Header("Audio Clip")]
    public AudioClip TitleScreen_Audio;
    public AudioClip BGM1;
    public AudioClip BGM2;
    public AudioClip ChestOpen;
    public AudioClip Click;
    public AudioClip Correct;
    public AudioClip Explosion;
    public AudioClip GlassBreak;
    public AudioClip Pickup;
    public AudioClip Search;
    public AudioClip Throw;
    public AudioClip Walk;
    public AudioClip WaterDrop;

    public string currentSceneName;

    public void Start()
    {
        currentSceneName = SceneManager.GetActiveScene().name;
        if (currentSceneName == "MainMenu")
        {
            PlayBGM(TitleScreen_Audio);
        }
        if (currentSceneName == "GGJ")
        {
            PlayBGM(BGM1);
        }
        else
        {
            PlayBGM(TitleScreen_Audio);
        }
    }
    public void PlayBGM(AudioClip clip)
    {
        musicSource.clip = clip;
        musicSource.Play();
    }

    public void PlaySFX(AudioClip clip)
    {
        SFXSource.PlayOneShot(clip);
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public Animator anim;

    public float speed = 10;
    private Vector2 lastClickedPos;
    private bool moving;

    private Rigidbody2D body;
    private SpriteRenderer spriteRenderer;

    public List<bool> conditions = new();
    private Camera mainCamera;
    private bool hitInteract = false;

    [SerializeField]
    private Transform grabPoint;
    [SerializeField]
    private Transform dropPoint;

    private GameObject grabbedObject;

    AudioManager audioManager;


    private void Start()
    {
        body = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }
    private void Awake()
    {
        mainCamera = Camera.main;
        audioManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioManager>();
    }
    public void OnClick(InputAction.CallbackContext context)
    {
        if (!context.started) return;
        audioManager.PlaySFX(audioManager.Click);

        var rayHit = Physics2D.GetRayIntersection(mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue()));
        if (!rayHit.collider) return;
        if (rayHit.collider.tag == "Path")
        {
            PlayerMove();
            audioManager.PlaySFX(audioManager.Walk);
        }
        if (rayHit.collider.tag == "Interactable")
        {
            rayHit.collider.gameObject.GetComponent<Interactable>().Interact();
            PlayerMove();
            hitInteract = true;
            audioManager.PlaySFX(audioManager.Search);
        }
        if (rayHit.collider.tag == "InteractableQuest")
        {
            if (rayHit.collider.gameObject.name.Contains("fountain"))
            {
                rayHit.collider.gameObject.GetComponent<Interact_Fountain>().Interact(this);
                PlayerMove();
            }
            if (rayHit.collider.gameObject.name.Contains("ticket"))
            {
                rayHit.collider.gameObject.GetComponent<Interact_Ticket>().Interact(this);
            }
            hitInteract = true;
        }
        if (rayHit.collider.tag == "Item")
        {
            if (grabbedObject != null)
            {
                DropObject(grabbedObject);
                grabbedObject = null;
            }
            else if (grabbedObject == null)
            {
                grabbedObject = rayHit.collider.gameObject;
                PickupObject(grabbedObject);
                audioManager.PlaySFX(audioManager.Pickup);
                if (grabbedObject.name.Contains("Panty"))
                {
                    GameManager.Instance.UpdateGameState(GameManager.GameState.Win);
                    DestroyGrabbedObject();
                    audioManager.PlaySFX(audioManager.Correct);
                }
            }
        }
    }
    public void OnPause(InputAction.CallbackContext context)
    {
        GameManager.Instance.UpdateGameState(GameManager.GameState.Pause);
    }

    private void Update()
    {
        if (moving && (Vector2)transform.position != lastClickedPos)
        {
            anim.SetBool("isWalk", true);

            lastClickedPos = new Vector2(lastClickedPos.x, lastClickedPos.y);
            float step = speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, lastClickedPos, step);

            body.velocity = new Vector2(lastClickedPos.x - transform.position.x, 0);
            spriteRenderer.flipX = body.velocity.x > 0f;
        }
        else
        {
            moving = false;

        }
        if (!moving)
        {
            anim.SetBool("isWalk", false);

            spriteRenderer.flipX = body.velocity.x < lastClickedPos.x;
            if (hitInteract)
            {
                anim.SetTrigger("isSearch");
                hitInteract = false;
            }
        }
    }
    public void PlayerMove()
    {
        lastClickedPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        moving = true;
    }

    public void PickupObject(GameObject grabbedObject)
    {
        if (grabbedObject != null && grabPoint != null)
        {
            grabbedObject.transform.position = grabPoint.position;
            grabbedObject.transform.SetParent(transform);
            Debug.Log("Object picked up. Parent: " + grabbedObject.transform.parent.name);
        }
    }
    public void DropObject(GameObject grabbedObject)
    {
        grabbedObject.transform.position = dropPoint.position;
        grabbedObject.transform.SetParent(null);
        grabbedObject = null;
    }

    public GameObject GetGrabbedObject()
    {
        return grabbedObject;
    }
    public void SetGrabbedObject(GameObject grabbedObject)
    {
        grabbedObject = this.grabbedObject; 
    }
    public void DestroyGrabbedObject()
    {
        Destroy(grabbedObject);
    }
}

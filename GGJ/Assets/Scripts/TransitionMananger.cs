using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static UnityEngine.SceneManagement.SceneManager;

public class TransitionMananger : MonoBehaviour
{
    [SerializeField] private GameObject _startScnTransition;
    [SerializeField] private GameObject _endScnTransition;
    
    // Start is called before the first frame update
    void Start()
    {
        _startScnTransition.SetActive(true);
        Invoke(nameof(DisableStartTransition), 3f);
    }
    
    private void DisableStartTransition()
    {
        _startScnTransition.SetActive(false);
    }
    
    private void DisableEndTransition()
    {
        _endScnTransition.SetActive(false);
    }

    public void OnClick(string sceneName)
    {
        _endScnTransition.SetActive(true);
        Invoke(nameof(DisableEndTransition), 2f);
        StartCoroutine(LoadScene(sceneName));
    }
    
    public void OnClickExit()
    {
        Application.Quit();
    }
    
    IEnumerator LoadScene(string scene)
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(scene);
    }
}

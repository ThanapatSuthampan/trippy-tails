using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TriggerDead : MonoBehaviour
{
    private PlayerMovement player;
    // GameObject ui;
    
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void OnClick(InputAction.CallbackContext context)
    {
        if (!context.started) return;

        var rayHit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue()));
        if (rayHit.collider) return;

        Debug.Log(rayHit.collider.gameObject.name);
    }
    
    /*public void OnMouseOver()
    {
        ui.SetActive(true);
    }

    public void OnMouseExit()
    {
        ui.SetActive(false);
    }*/
}

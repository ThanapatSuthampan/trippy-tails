using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Interact_Fountain : MonoBehaviour
{
    public GameObject ui;
    public GameObject rewardObj;
    public Transform dropPos;
    public Animator TK_anim;
    AudioManager audioManager;

    private PolygonCollider2D _collider2D;

    private void Awake()
    {
        _collider2D = GetComponent<PolygonCollider2D>();
        audioManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioManager>();
        audioManager.GetComponent<AudioManager>();
    }
    public void OnMouseOver()
    {
        ui.SetActive(true);
    }

    public void OnMouseExit()
    {
        ui.SetActive(false);
    }
    public void Interact(PlayerMovement player)
    {
        GameObject grabbedObject = player.GetGrabbedObject();

        if (grabbedObject != null)
        {
            if (grabbedObject.name.Contains("Coin"))
            {
                StartCoroutine(InteractTimer());
                player.SetGrabbedObject(null);
                player.DestroyGrabbedObject();
                audioManager.PlaySFX(audioManager.WaterDrop);
            }
            if (grabbedObject.name.Contains("poop"))
            {
                GameManager.Instance.UpdateGameState(GameManager.GameState.GameOver);
                player.SetGrabbedObject(null);
                player.DestroyGrabbedObject();
                audioManager.PlaySFX(audioManager.WaterDrop);
            }
            else
            {
                Debug.Log("Wrong Object.");
            }
        }
    }
    private IEnumerator InteractTimer()
    {
        Debug.Log("Ineract!");
        yield return new WaitForSeconds(1f);
        Instantiate(rewardObj, dropPos.position, Quaternion.identity);
        yield return new WaitForSeconds(1f);
        _collider2D.enabled = false;
    }
}

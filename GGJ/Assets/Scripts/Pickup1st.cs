using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup1st : MonoBehaviour
{
    private PlayerMovement _player;
    
    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<PlayerMovement>();
    }

    private IEnumerator OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _player.conditions[0] = true;
        }
        
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}

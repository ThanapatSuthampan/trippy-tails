using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ticket_Bahaviour : MonoBehaviour
{
    public GameObject ui;
    
    private PlayerMovement player;
    public Animator TK_anim;
    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
        TK_anim = GetComponent<Animator>();
        gameManager = GameManager.Instance;
    }
    private void Update()
    {
        if (gameManager.State == GameManager.GameState.Win)
        {
            TK_anim.SetTrigger("Laugh");
        }
    }

    public void OnMouseOver()
    {
        ui.SetActive(true);
    }

    public void OnMouseExit()
    {
        ui.SetActive(false);
    }
}

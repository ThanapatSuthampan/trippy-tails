using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class GameManager : MonoBehaviour
{
    [Header("Instance")]
    public static GameManager Instance;

    [Header("State")]
    public GameState State;
    public static event Action<GameState> OnGameStateChanged;

    [Header("Reference")]
    public PlayerMovement playerMovement;
    public PlayerInput playerInput;
    public TutorialScript tutorialScript;
    public PauseScript pauseScript;
    public GamerOverScript gamerOverScript;

    // Start is called before the first frame update
    public void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        UpdateGameState(GameState.PreStart);
    }
    public void UpdateGameState(GameState newState)
    {
        State = newState;

        switch (newState)
        {
            case GameState.PreStart:
                HandlePreStart();
                StartCoroutine(WaitForMouseClick());
                break;
            case GameState.Pause:
                HandlePause();
                break;
            case GameState.Unpause:
                HandleUnPause();
                break;
            case GameState.Start:
                break;
            case GameState.Win:
                HandleWin();
                break;
            case GameState.GameOver:
                HandleGameOver();
                break;
            default:
                break;
        }

        OnGameStateChanged?.Invoke(newState);
    }

    private void HandleGameOver()
    {
        gamerOverScript.Enable();
        gamerOverScript.GetSceneName();
        playerMovement.enabled = false;
        playerInput.enabled = false;
    }

    private void HandleWin()
    {
        playerMovement.enabled = false;
        playerInput.enabled = false;
        playerMovement.anim.SetTrigger("win");
    }

    private void HandleUnPause()
    {
        playerMovement.enabled = true;
        playerInput.enabled = true;
        pauseScript.Disable();
    }

    private void HandlePause()
    {
        playerMovement.enabled = false;
        playerInput.enabled = false;
        pauseScript.Enable();
        pauseScript.GetSceneName();
    }

    public void HandlePreStart()
    {
        playerMovement.enabled = false;
        tutorialScript.Enable();
    }
    private IEnumerator WaitForMouseClick()
    {
        yield return new WaitUntil(() => Mouse.current.leftButton.isPressed);
        tutorialScript.Disable();
        playerMovement.enabled = true;
        UpdateGameState(GameState.Start);
    }

    public enum GameState
    {
        PreStart,
        Pause,
        Unpause,
        Start,
        Win,
        GameOver
    }
}
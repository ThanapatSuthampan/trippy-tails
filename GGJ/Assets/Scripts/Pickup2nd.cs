using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup2nd : MonoBehaviour
{
    private PlayerMovement _player;
    
    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<PlayerMovement>();
    }

    private IEnumerator OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _player.conditions[1] = true;
            _player.conditions[0] = false;
        }
        
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}

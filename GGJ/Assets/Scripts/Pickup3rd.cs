using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup3rd : MonoBehaviour
{
    private PlayerMovement _player;
    private float Potuay;
    
    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<PlayerMovement>();
    }

    private IEnumerator OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!_player.conditions[1])
            {
                _player.conditions[2] = true;
            }
        }
        
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamerOverScript : MonoBehaviour
{
    public string currentSceneName;
    public void Enable()
    {
        gameObject.SetActive(true);
    }
    public void Disable()
    {
        gameObject.SetActive(false);
    }
    public void GetSceneName()
    {
        currentSceneName = SceneManager.GetActiveScene().name;
    }
    public void OnRestart()
    {
        SceneManager.LoadScene(currentSceneName);
    }
}

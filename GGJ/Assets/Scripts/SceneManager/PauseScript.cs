using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour
{
    public string currentSceneName;
    public void GetSceneName()
    {
        currentSceneName = SceneManager.GetActiveScene().name;
    }
    public void Enable()
    {
        gameObject.SetActive(true);
    }
    public void Disable()
    {
        gameObject.SetActive(false);
    }
    public void OnRestart()
    {
        SceneManager.LoadScene(currentSceneName);
    }
    public void OnContinue()
    {
        GameManager.Instance.UpdateGameState(GameManager.GameState.Start);
    }

    public void OnOption()
    {

    }

    public void OnMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void OnExit()
    {
        GameManager.Instance.UpdateGameState(GameManager.GameState.Unpause);
    }
}

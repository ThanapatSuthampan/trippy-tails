using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Interactable : MonoBehaviour
{
    public GameObject ui;
    public GameObject rewardObj;
    public Transform dropPos;

    private PolygonCollider2D _collider2D;
    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
        _collider2D = GetComponent<PolygonCollider2D>();
    }
    public void OnMouseOver()
    {
        ui.SetActive(true);
    }

    public void OnMouseExit()
    {
        ui.SetActive(false);
    }
    public void Interact()
    {
        StartCoroutine(InteractTimer());
    }

    private IEnumerator InteractTimer()
    {
        Debug.Log("Ineract!");
        yield return new WaitForSeconds(1f);
        Instantiate(rewardObj, dropPos.position, Quaternion.identity);
        yield return new WaitForSeconds(1f);
        _collider2D.enabled = false;
    }
}

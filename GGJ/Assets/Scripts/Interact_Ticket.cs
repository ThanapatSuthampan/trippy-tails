using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact_Ticket : MonoBehaviour
{
    public GameObject ui;

    private BoxCollider2D _collider2D;
    private GameManager gameManager;
    public Animator TK_anim;
    AudioManager audioManager;
    private void Awake()
    {
        _collider2D = GetComponent<BoxCollider2D>();
        audioManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioManager>();
    }
    private void Start()
    {
        gameManager = GameManager.Instance;
        TK_anim = GetComponent<Animator>();
    }
    public void OnMouseOver()
    {
        ui.SetActive(true);
    }

    public void OnMouseExit()
    {
        ui.SetActive(false);
    }
    public void Update()
    {
        if (gameManager.State == GameManager.GameState.Win)
        {
            TK_anim.SetTrigger("Laugh");
        }
    }
    public void Interact(PlayerMovement player)
    {
        Debug.Log("Interact!!!!");
        GameObject grabbedObject = player.GetGrabbedObject();

        if (grabbedObject != null)
        {
            if (grabbedObject.name.Contains("poop"))
            {
                player.anim.SetTrigger("throw");
                StartCoroutine(InteractTimer());
                player.SetGrabbedObject(null);
                player.DestroyGrabbedObject();
                audioManager.PlaySFX(audioManager.Throw);
            }
            else
            {
                Debug.Log("Wrong Object.");
            }
        }
    }
    private IEnumerator InteractTimer()
    {
        Debug.Log("Ineract!");
        yield return new WaitForSeconds(2f);
        GameManager.Instance.UpdateGameState(GameManager.GameState.GameOver);
    }
}
